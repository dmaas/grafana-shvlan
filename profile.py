#!/usr/bin/env python

import os

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as ig
import geni.rspec.emulab.pnext as PN


tourDescription = """
### Grafana/Loki instance with dataset

"""

tourInstructions = """

- [The Grafana web interface](http://{host-server}:3000/) (user `admin`, password `{password-perExptPassword}`)

Startup scripts will still be running when your experiment becomes ready.
Watch the "Startup" column on the "List View" tab for your experiment and wait
until all of the compute nodes show "Finished" before proceeding.

"""

BIN_PATH = "/local/repository/bin"
ETC_PATH = "/local/repository/etc"
UBUNTU_JAMMY_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"
TEST_TOOLS_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-test-tools.sh")


pc = portal.Context()

node_types = [
    ("d430", "Emulab, d430"),
    ("d740", "Emulab, d740"),
]
pc.defineParameter(
    name="node_type",
    description="Type of compute node to use for Grafana/Loki server",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types
)

pc.defineParameter(
    name="remote_data_source",
    description="connect a remote data source",
    typ=portal.ParameterType.STRING,
    defaultValue=""
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

server = request.RawPC("server")
server.component_manager_id = COMP_MANAGER_ID
server.hardware_type = params.node_type
server.disk_image = UBUNTU_JAMMY_IMG
if params.remote_data_source:
    iface = server.addInterface()
    fsnode = request.RemoteBlockstore("fsnode", "/data")
    fsnode.dataset = params.remote_data_source
    fsnode.rwclone = True
    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)
    fslink.best_effort = True
    fslink.vlan_tagging = True

server.addService(rspec.Execute(shell="bash", command=TEST_TOOLS_DEPLOY_SCRIPT))

tour = ig.Tour()
tour.Description(ig.Tour.MARKDOWN, tourDescription)
tour.Instructions(ig.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

# Password for grafana.
request.addResource(ig.Password("perExptPassword"))

pc.printRequestRSpec(request)
